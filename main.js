// Removed the 'input' event listener on 'api-key'

document.getElementById("api-form").addEventListener("submit", function (e) {
  e.preventDefault();
  // Here you can handle the API key submission
});

document.getElementById("load-img-btn").addEventListener("click", function () {
  // Here you can handle the image loading from the NASA APOD API
  fetch(
    "https://api.nasa.gov/planetary/apod?api_key=" +
      document.getElementById("api-key").value,
  )
    .then((response) => response.json())
    .then((data) => (document.getElementById("apod-img").src = data.url));
});
